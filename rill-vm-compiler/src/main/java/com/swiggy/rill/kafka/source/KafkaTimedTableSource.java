package com.swiggy.rill.kafka.source;

import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.connectors.kafka.Kafka09JsonTableSource;
import org.apache.flink.table.sources.DefinedProctimeAttribute;
import org.apache.flink.types.Row;

import java.util.Properties;

public class KafkaTimedTableSource extends Kafka09JsonTableSource implements DefinedProctimeAttribute {

    String proctimeAttribute;

    public KafkaTimedTableSource(String topic, Properties properties, TypeInformation<Row> typeInfo , String proctimeAttribute) {
        super(topic, properties, typeInfo);
        this.proctimeAttribute = proctimeAttribute;
    }


    @Override
    public String getProctimeAttribute() {
        return proctimeAttribute;
    }

}
