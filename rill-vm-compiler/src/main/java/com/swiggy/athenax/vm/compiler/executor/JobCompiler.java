/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.swiggy.athenax.vm.compiler.executor;

import com.google.gson.Gson;
import com.swiggy.athenax.backend.server.AthenaXConfiguration;
import com.swiggy.athenax.vm.api.AthenaXAggregateFunction;
import com.swiggy.athenax.vm.api.AthenaXScalarFunction;
import com.swiggy.athenax.vm.api.AthenaXTableFunction;
import com.swiggy.athenax.vm.api.DataSinkProvider;
import com.swiggy.rill.kafka.source.KafkaTimedTableSource;
import com.uber.athenax.backend.server.CoreContext;
import com.uber.athenax.vm.compiler.executor.ColumnSchemaToCatalogSchemaMapper;
import com.uber.athenax.vm.compiler.executor.ColumnType;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.runtime.jobgraph.JobGraph;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.graph.StreamGraph;
import org.apache.flink.streaming.connectors.kafka.*;
import org.apache.flink.streaming.connectors.kafka.partitioner.FlinkFixedPartitioner;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.Types;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.catalog.ExternalCatalogTable;
import org.apache.flink.table.functions.AggregateFunction;
import org.apache.flink.table.functions.ScalarFunction;
import org.apache.flink.table.functions.TableFunction;
import org.apache.flink.table.sinks.AppendStreamTableSink;
import org.apache.flink.types.Row;
import org.apache.flink.util.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class JobCompiler {
    private static final Logger LOG = LoggerFactory.getLogger(JobCompiler.class);
    private final StreamTableEnvironment env;
    private final JobDescriptor job;
    private Connection connection;

    private Properties properties;

    /*{
        properties.setProperty("bootstrap.servers", "localhost:9092");
        properties.setProperty("enable.auto.commit", "true");
        properties.setProperty("group.id", "athenax");
    }*/

    JobCompiler(StreamTableEnvironment env, JobDescriptor job, AthenaXConfiguration athenaXConfiguration) throws IOException {
        this.job = job;
        this.env = env;
        properties = new Properties();
        properties.putAll(athenaXConfiguration.kafkaConfig);
    }

    public static CompilationResult main(JobDescriptor job) throws IOException, SQLException {
        StreamExecutionEnvironment execEnv = StreamExecutionEnvironment.createLocalEnvironment();
        execEnv.getConfig().enableSysoutLogging();
        execEnv.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);
        StreamTableEnvironment env = StreamTableEnvironment.getTableEnvironment(execEnv);
        CompilationResult res = new CompilationResult();

        res.setJobGraph(new JobCompiler(env, job, job.getConfig()).getJobGraph());

        return res;
    }

    private static OutputStream chooseOutputStream(String[] args) throws IOException {
        if (args.length > 0) {
            int port = Integer.parseInt(args[0]);
            Socket sock = new Socket();
            sock.connect(new InetSocketAddress(InetAddress.getLocalHost(), port));
            return sock.getOutputStream();
        } else {
            return System.out;
        }
    }

    JobGraph getJobGraph() throws IOException, SQLException {

        FlinkFixedPartitioner partition = new FlinkFixedPartitioner();
        StreamExecutionEnvironment exeEnv = env.execEnv();
        exeEnv.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);
        exeEnv.setParallelism(job.parallelism());
        this
                .registerUdfs()
                .registerInputCatalogs();
        Table table = env.sql(job.sql());
        for (String t : job.outputs()) {
            KafkaJsonTableSink kafkaTableSink = new Kafka09JsonTableSink(
                    t,
                    properties,
                    partition
            );
            System.out.println("table.getSchema()" + table.getSchema().toString());
            table.writeToSink(kafkaTableSink);
        }

        StreamGraph streamGraph = exeEnv.getStreamGraph();
        return streamGraph.getJobGraph();
    }

    static JobDescriptor getJobConf(InputStream is) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(is)) {
            return (JobDescriptor) ois.readObject();
        }
    }

    private JobCompiler registerUdfs() {
        for (Map.Entry<String, String> e : job.udf().entrySet()) {
            final String name = e.getKey();
            String clazzName = e.getValue();
            final Object udf;

            try {
                Class<?> clazz = Class.forName(clazzName);
                udf = clazz.newInstance();
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
                throw new IllegalArgumentException("Invalid UDF " + name, ex);
            }

            if (udf instanceof AthenaXScalarFunction) {
                env.registerFunction(name, (ScalarFunction) udf);
            } else if (udf instanceof AthenaXTableFunction) {
                env.registerFunction(name, (TableFunction<?>) udf);
            } else if (udf instanceof AthenaXAggregateFunction) {
                env.registerFunction(name, (AggregateFunction<?, ?>) udf);
            } else {
                LOG.warn("Unknown UDF {} was found.", clazzName);
            }
        }
        return this;
    }

    private JobCompiler registerInputCatalogs() throws SQLException {
        //TODO make proctime dynamic
        final String procTime = "time_stamp";
        for (String e : job.inputs()) {
            String schema = null;
            LOG.debug("Registering input catalog {}", e);
            this.connection = CoreContext.INSTANCE.getMysqlConnection();


                Statement stmt = connection.createStatement();
                ResultSet rs=stmt.executeQuery("select topic_schema from topic_schema where topic_name = \""+ e +"\"");
                if(rs.next()) schema = rs.getString("topic_schema");

            Gson gson = new Gson();
            ColumnType[] columnTypeList = gson.fromJson(schema, ColumnType[].class);

            List<String> columns = getColumnList(columnTypeList , procTime);
            String[] columnsArray = new String[columns.size()];
            columnsArray = columns.toArray(columnsArray);
            List<TypeInformation> typeInformationList = getTypeInformationList(columnTypeList, procTime);
            TypeInformation[] typeInformationArray = new TypeInformation[typeInformationList.size()];
            typeInformationArray = typeInformationList.toArray(typeInformationArray);

            TypeInformation<Row> typeInfo = Types.ROW(
                    columnsArray,
                    typeInformationArray
            );

            KafkaJsonTableSource kafkaTableSource = new Kafka09JsonTableSource(
                    e,
                    properties,
                    typeInfo);



            env.registerTableSource(e, kafkaTableSource);
        }
        return this;
    }

    private List<TypeInformation> getTypeInformationList(ColumnType[] columnTypeList, String procTime) {
        List<TypeInformation> typeInformationList = new ArrayList<TypeInformation>();
        ColumnSchemaToCatalogSchemaMapper columnSchemaToCatalogSchemaMapper = new ColumnSchemaToCatalogSchemaMapper();
        for(ColumnType columnType: columnTypeList){
            if(!columnType.column.equals(procTime))
            typeInformationList.add(columnSchemaToCatalogSchemaMapper.getTypeInformation(columnType));
        }
        return typeInformationList;
    }

    private List<String> getColumnList(@Nonnull ColumnType[] columnTypeList , String procTime) {
        List<String> columnList = new ArrayList<String>();
        for(ColumnType columnType: columnTypeList){
            if(!columnType.column.equals(procTime))
            columnList.add(columnType.column);
        }
        return columnList;
    }

    private AppendStreamTableSink<Row> getOutputTable(
            ExternalCatalogTable output) throws IOException {
        String tableType = output.tableType();
        DataSinkProvider c = DataSinkRegistry.getProvider(tableType);
        Preconditions.checkNotNull(c, "Cannot find output connectors for " + tableType);
        return c.getAppendStreamTableSink(output);
    }
}
