package com.uber.athenax.vm.compiler.executor;

import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.table.api.Types;
import org.apache.flink.table.typeutils.TimeIndicatorTypeInfo;

import java.util.HashMap;
import java.util.Map;

public class ColumnSchemaToCatalogSchemaMapper {
    public Map<String, TypeInformation> columnTypeInformationMap = new HashMap<>();

    public ColumnSchemaToCatalogSchemaMapper(){
        columnTypeInformationMap.put("float", Types.FLOAT());
        columnTypeInformationMap.put("int", Types.INT());
//        columnTypeInformationMap.put("proctime", TimeIndicatorTypeInfo.PROCTIME_INDICATOR());
        columnTypeInformationMap.put("string", Types.STRING());
        columnTypeInformationMap.put("byte", Types.BYTE());
        columnTypeInformationMap.put("boolean", Types.BOOLEAN());
        columnTypeInformationMap.put("decimal", Types.DECIMAL());
        columnTypeInformationMap.put("double", Types.DOUBLE());
        columnTypeInformationMap.put("long", Types.LONG());
        columnTypeInformationMap.put("sql_date", Types.SQL_DATE());
        columnTypeInformationMap.put("sql_time", Types.SQL_TIME());
        columnTypeInformationMap.put("sql_timestamp", Types.SQL_TIMESTAMP());
        columnTypeInformationMap.put("interval_millis", Types.INTERVAL_MILLIS());
        columnTypeInformationMap.put("interval_months", Types.INTERVAL_MONTHS());
    }

    public TypeInformation getTypeInformation(ColumnType columnType) throws RuntimeException{
        if(columnTypeInformationMap.containsKey(columnType.type.toLowerCase())) return columnTypeInformationMap.get(columnType.type);
        else throw new RuntimeException("Column type " + columnType.type + " is not supported"+ "column is"+ columnType.column);
    }
}
