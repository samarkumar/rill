# Rill: SQL-based streaming analytics platform at scale

Rill is a streaming analytics platform that enables users to run production-quality, large scale streaming analytics using Structured Query Language (SQL). It is capable of scaling across hundreds of machines and processing hundreds of billions of real-time events daily.
