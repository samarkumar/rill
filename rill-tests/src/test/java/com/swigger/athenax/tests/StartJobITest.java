/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.swigger.athenax.tests;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.swiggy.athenax.backend.api.JobDefinition;
import com.swiggy.athenax.backend.api.JobDefinitionDesiredstate;
import com.swiggy.athenax.backend.api.JobDefinitionResource;
import com.swiggy.athenax.backend.api.client.Configuration;
import com.swiggy.athenax.backend.server.ServerContext;
import com.swiggy.athenax.backend.server.WebServer;
import com.swiggy.athenax.backend.api.client.Configuration;
import com.swiggy.athenax.backend.api.client.JobsApi;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.junit.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class StartJobITest {
    private static final Logger LOG = LoggerFactory.getLogger(StartJobITest.class);
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Test
    public void testStartJob() throws Exception {

        {
            ITestUtil.initSetup();
            Configuration.getDefaultApiClient().setBasePath(String.format("http://localhost:%d%s", 8083, WebServer.BASE_PATH));
            LOG.info("AthenaX server listening on http://localhost:{}", "8083");

            List<String> inputs = new LinkedList<>();
            List<String> output = new LinkedList<String>();
            inputs.add("input1");
            output.add("output1");
            JobsApi api = new JobsApi();
            api.listJob();
//            String uuid = api.allocateNewJob().getJobUuid();
            JobDefinitionDesiredstate state = new JobDefinitionDesiredstate()
                    .clusterId("foo")
                    .resource(new JobDefinitionResource().vcores(1L).memory(2048L));
            JobDefinition job = new JobDefinition()
                    .query("SELECT  count(*) ,id  FROM input1 GROUP BY HOP(proctime , INTERVAL '2' MINUTE, INTERVAL '1' MINUTE) , id")
                    .addDesiredStateItem(state);
            job.setInputs(inputs);
            job.setOutputs(output);
            String uuid = api.allocateNewJob(job).getJobUuid();
//            api.updateJob(UUID.fromString(uuid), job);

            try (KafkaConsumer<byte[], byte[]> consumer = ITestUtil.getConsumer("observer", ITestUtil.brokerAddress())) {
                consumer.subscribe(Collections.singletonList(ITestUtil.DEST_TOPIC));
                boolean found = false;
                while (!found) {
                    ConsumerRecords<byte[], byte[]> records = consumer.poll(1000);
                    for (ConsumerRecord<byte[], byte[]> r : records.records(ITestUtil.DEST_TOPIC)) {
                        System.out.println(r.toString());
                    }
                }
                ServerContext.INSTANCE.executor().shutdown();
                ServerContext.INSTANCE.instanceManager().close();
            }
        }
    }
}

