package com.uber.athenax.backend.server;

import com.swiggy.athenax.backend.server.AthenaXConfiguration;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.apache.flink.configuration.ConfigOptions.key;

public final class CoreContext {
    public static final CoreContext INSTANCE = new CoreContext();
    private Connection mysqlConnection;
    private ZkClient zkClient;
    private ZkUtils zkUtils;

    public void initialize(AthenaXConfiguration configuration) throws ClassNotFoundException, IOException {
        this.mysqlConnection = instantiateMysqlConnection(configuration);
        this.zkClient = instantiateZkClient(configuration);
        this.zkUtils = instantiateZkUtils(configuration);
    }

    private ZkUtils instantiateZkUtils(AthenaXConfiguration configuration) {
        String zookeeperHosts = (String) configuration.zookeeper().get(key("hosts").noDefaultValue().key());
        return new ZkUtils(zkClient, new ZkConnection(zookeeperHosts), false);
    }

    private ZkClient instantiateZkClient(AthenaXConfiguration configuration) {
//        ZkClient zkClient = null;
        String zookeeperHosts = (String) configuration.zookeeper().get(key("hosts").noDefaultValue().key());
        int sessionTimeOutInMs = (Integer) configuration.zookeeper().get(key("sessionTimeOut").noDefaultValue().key()); // 15 secs
        int connectionTimeOutInMs = (Integer) configuration.zookeeper().get(key("connectionTimeOut").noDefaultValue().key()); // 10 secs
        return new ZkClient(zookeeperHosts, sessionTimeOutInMs, connectionTimeOutInMs, ZKStringSerializer$.MODULE$);
    }

    private Connection instantiateMysqlConnection(AthenaXConfiguration configuration)
            throws ClassNotFoundException, IOException {
        Connection connection = null;
        String server = (String) configuration.mysql().get(key("server").noDefaultValue().key());
        String database = (String) configuration.mysql().get(key("database").noDefaultValue().key());
        String userName = (String) configuration.mysql().get(key("user").noDefaultValue().key());
        String password = (String) configuration.mysql().get(key("password").noDefaultValue().key());
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:mysql://"+server+"/"+database, userName, password);
        }catch(SQLException e){ throw new IOException(e);}
        return connection;
    }

    public Connection getMysqlConnection() { return mysqlConnection; }

    public ZkClient getZkClient() {
        return zkClient;
    }

    public ZkUtils getZkUtils() {
        return zkUtils;
    }
}
