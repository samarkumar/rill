package config.clazz;

import org.apache.flink.api.common.typeinfo.BasicTypeInfo;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.table.api.TableSchema;
import org.apache.flink.table.catalog.ExternalCatalogTable;

import java.io.Serializable;
import java.util.Map;

public class KafkaInputExternalCatalogTable extends ExternalCatalogTable implements Serializable {
    private static final TableSchema SCHEMA = new TableSchema(
            new String[] {"id"},
            new TypeInformation<?>[] {BasicTypeInfo.INT_TYPE_INFO});

    KafkaInputExternalCatalogTable(Map<String, String> properties) {
        super("kafka+json", SCHEMA, properties, null, null, null, null);
    }
}