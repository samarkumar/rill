/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.swiggy.athenax.backend.server.jobs;

import com.swiggy.athenax.backend.api.JobDefinitionDesiredstate;
import com.swiggy.athenax.backend.server.yarn.InstanceInfo;
import com.swiggy.athenax.backend.api.ExtendedJobDefinition;
import com.swiggy.athenax.backend.api.JobDefinition;
import com.swiggy.athenax.backend.api.JobDefinitionResource;
import com.swiggy.athenax.backend.api.ExtendedJobDefinition;
import com.swiggy.athenax.backend.api.JobDefinition;
import com.swiggy.athenax.backend.api.JobDefinitionDesiredstate;
import com.swiggy.athenax.backend.api.JobDefinitionResource;
import org.apache.hadoop.yarn.api.records.YarnApplicationState;

import java.io.IOException;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.apache.hadoop.yarn.api.records.YarnApplicationState.ACCEPTED;
import static org.apache.hadoop.yarn.api.records.YarnApplicationState.NEW;
import static org.apache.hadoop.yarn.api.records.YarnApplicationState.NEW_SAVING;
import static org.apache.hadoop.yarn.api.records.YarnApplicationState.RUNNING;
import static org.apache.hadoop.yarn.api.records.YarnApplicationState.SUBMITTED;

public final class JobWatcherUtil {
    private static final EnumSet<YarnApplicationState> ALIVE_STATE =
            EnumSet.of(NEW, NEW_SAVING, SUBMITTED, ACCEPTED, RUNNING);

    private JobWatcherUtil() {
    }

    static final class StateView {
        // List of jobs
        private final Map<UUID, JobDefinition> jobs;
        // List of instances
        private final Map<UUID, InstanceInfo> instances;
        // Instance ID -> JobDefinition ID
        private final Map<UUID, UUID> instanceToJob;
        // JobDefinition ID -> List<Instance>
        private final Map<UUID, List<InstanceInfo>> jobInstances;

        private StateView(Map<UUID, JobDefinition> jobs,
                          Map<UUID, InstanceInfo> instances,
                          Map<UUID, UUID> instanceToJob,
                          Map<UUID, List<InstanceInfo>> jobInstances) {
            this.jobs = jobs;
            this.instances = instances;
            this.instanceToJob = instanceToJob;
            this.jobInstances = jobInstances;
        }

        Map<UUID, JobDefinition> jobs() {
            return jobs;
        }

        Map<UUID, InstanceInfo> instances() {
            return instances;
        }

        Map<UUID, UUID> instanceToJob() {
            return instanceToJob;
        }

        Map<UUID, List<InstanceInfo>> jobInstances() {
            return jobInstances;
        }
    }

    static HealthCheckReport computeHealthCheckReport(Map<UUID, JobDefinition> jobs,
                                                      Map<UUID, InstanceInfo> instances) {
        HealthCheckReport res = new HealthCheckReport();

        for (Map.Entry<UUID, JobDefinition> eachJobs : jobs.entrySet()) {

            if(instances.containsKey(eachJobs.getKey())) {
                YarnApplicationState state = YarnApplicationState.valueOf(instances.get(eachJobs.getKey()).status().getState().toString());
                if (isInstanceAlive(state)) {
                    continue;
                }
            }

            res.instancesToStart().put(
                    new ExtendedJobDefinition().uuid(eachJobs.getKey())
                            .definition(eachJobs.getValue()),
                    eachJobs.getValue().getDesiredState());

        }


        return res;
    }


    static JobDefinitionDesiredstate computeActualState(InstanceInfo info) {
        JobDefinitionResource r = new JobDefinitionResource()
                .memory(info.status().getAllocatedMB())
                .vcores(info.status().getAllocatedVCores());
        JobDefinitionDesiredstate s = new JobDefinitionDesiredstate()
                .clusterId(info.clusterName())
                .resource(r);
        return s;
    }

    static HashMap<UUID, JobDefinition> listJobs(JobStore jobStore) throws IOException {
        List<ExtendedJobDefinition> jobs = jobStore.listAll();
        HashMap<UUID, JobDefinition> results = new HashMap<>();
        jobs.forEach(x -> results.put(x.getUuid(), x.getDefinition()));
        return results;
    }

    public static boolean isInstanceAlive(YarnApplicationState state) {
        return ALIVE_STATE.contains(state);
    }
}
