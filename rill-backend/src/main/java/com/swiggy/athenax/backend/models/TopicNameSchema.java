package com.swiggy.athenax.backend.models;

import lombok.Data;

@Data
public class TopicNameSchema {
    private String topicName;
    private String topicSchema;
}
