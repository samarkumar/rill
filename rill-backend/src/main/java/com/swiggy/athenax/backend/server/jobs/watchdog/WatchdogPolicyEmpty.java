/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.swiggy.athenax.backend.server.jobs.watchdog;

import com.swiggy.athenax.backend.api.JobDefinitionDesiredstate;
import com.swiggy.athenax.backend.server.yarn.InstanceInfo;
import com.swiggy.athenax.backend.server.yarn.InstanceManager;
import com.swiggy.athenax.backend.api.ExtendedJobDefinition;
import com.swiggy.athenax.backend.api.JobDefinition;
import com.swiggy.athenax.backend.server.ServerContext;
import com.swiggy.athenax.backend.server.jobs.HealthCheckReport;
import com.swiggy.athenax.backend.server.jobs.JobManager;
import com.swiggy.athenax.backend.server.jobs.WatchdogPolicy;
import com.swiggy.athenax.vm.compiler.planner.JobCompilationResult;
import org.apache.flink.shaded.com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class WatchdogPolicyEmpty implements WatchdogPolicy {
    private static final Logger LOG = LoggerFactory.getLogger(WatchdogPolicyEmpty.class);
    private final InstanceManager instanceManager = ServerContext.INSTANCE.instanceManager();
    private final JobManager jobManager = ServerContext.INSTANCE.jobManager();

    @Override
    public void onHealthCheckReport(HealthCheckReport report) {
        LOG.debug("WatchDog watching " + report.instancesToStart().size());

        for (InstanceInfo info : Iterables.concat(report.spuriousInstances(), report.instancesWithDifferentParameters())) {
            LOG.debug("Current spuriousInstances " + info.appId());

        }


        for (Map.Entry<ExtendedJobDefinition, List<JobDefinitionDesiredstate>> e : report.instancesToStart().entrySet()) {

            final JobCompilationResult res;
            JobDefinition job = e.getKey().getDefinition();
            JobDefinitionDesiredstate jobDefinitionDesiredstate = job.getDesiredState().get(0);
            try {

                res = jobManager.compile(job, jobDefinitionDesiredstate);
                instanceManager.instantiate(jobDefinitionDesiredstate, e.getKey().getUuid(), res);
            } catch (Throwable ex) {
                LOG.warn("Failed to instantiate the query '{}' on {}", job.getQuery(), jobDefinitionDesiredstate.getClusterId(), ex);
            }

        }

    }
}
