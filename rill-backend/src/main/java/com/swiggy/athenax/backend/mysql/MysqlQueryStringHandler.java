package com.swiggy.athenax.backend.mysql;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.athenax.backend.api.JobDefinition;

import java.util.UUID;

public final class MysqlQueryStringHandler {
//    public static final MysqlQueryStringHandler INSTANCE = new MysqlQueryStringHandler();
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final String TOPIC_SCHEMA = "topic_schema";
    private static final String JOB_DEFINITION = "job_definition";

    public static String getJobDefinitionFromUuid(UUID uuid){
        return "select * from "+JOB_DEFINITION+" where uuid = \""+uuid+"\"";
    }

    public static String insertOrUpdateJobDefinition(UUID uuid, JobDefinition job) throws JsonProcessingException {
        return "INSERT INTO "+JOB_DEFINITION+" (uuid, definition) VALUES(\""+uuid.toString()+"\", \""+
                MAPPER.writeValueAsString(job).replace("\"", "\\\"")+"\") " +
                "ON DUPLICATE KEY UPDATE " +
                "definition=\""+MAPPER.writeValueAsString(job).replace("\"", "\\\"")+"\"";
    }

    public static String deleteJobDefinition(UUID uuid){
        return "DELETE FROM "+JOB_DEFINITION+" where uuid = \""+uuid.toString()+"\"";
    }

    public static String insertOrUpdateTopicSchema(String topicName, String schema){
        return "INSERT INTO "+TOPIC_SCHEMA+" (topic_name, topic_schema) VALUES(\"" + topicName+"\", \""+
                schema.replace("\"", "\\\"")+"\") " +
                "ON DUPLICATE KEY UPDATE topic_schema=\""+schema.replace("\"", "\\\"")+"\"";
    }

    public static String updateTopicSchema(String sourceName, String schema){
        return "UPDATE "+TOPIC_SCHEMA+" SET topic_schema = \"" +
                schema.replace("\"", "\\\"") + "\" where topic_name = \""+
                sourceName+"\"";
    }

    public static String getTopicNameSchema(String topicName){
        return "select * from "+TOPIC_SCHEMA+" where topic_name = \""+ topicName +"\"";
    }

    public static String getUuidAndJobDefinition(){
        return "select uuid, definition from "+ JOB_DEFINITION;
    }

    public static String getTopicNameSchemaList(){
        return "select * from "+ TOPIC_SCHEMA;
    }

    public static String getTopicSchema(String topicName){
        return "select topic_schema from "+TOPIC_SCHEMA+" where topic_name = \""+ topicName +"\"";
    }
}
