package com.swiggy.athenax.backend.mysql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.athenax.backend.api.ExtendedJobDefinition;
import com.swiggy.athenax.backend.api.JobDefinition;
import com.swiggy.athenax.backend.server.AthenaXConfiguration;
import com.swiggy.athenax.backend.server.ServerContext;
import com.swiggy.athenax.backend.server.jobs.JobStore;
import java.io.IOException;
import java.sql.*;
import java.util.*;

public class MysqlDBJobStore implements JobStore {
    private Connection connection;
    private static final ObjectMapper MAPPER = new ObjectMapper();
    @Override
    public void open(AthenaXConfiguration conf) throws IOException {
       this.connection = ServerContext.INSTANCE.getMysqlConnection();
    }

    @Override
    public JobDefinition get(UUID uuid) throws IOException {
        byte[] jobDefinition = null;
        ResultSet rs = MysqlQueryHandler.executeQuery(MysqlQueryStringHandler.getJobDefinitionFromUuid(uuid));
        try {
            if(rs.next()) jobDefinition = rs.getBytes(2);
        } catch (SQLException e) {
            throw new IOException(e);
        }


        return MAPPER.readValue(jobDefinition, JobDefinition.class);
    }

    @Override
    public void updateJob(UUID uuid, JobDefinition job) throws IOException {
        String updateString = MysqlQueryStringHandler.insertOrUpdateJobDefinition(uuid, job);
        MysqlQueryHandler.execute(updateString);
    }

    @Override
    public void removeJob(UUID uuid) throws IOException {
        try{
            String updateString = MysqlQueryStringHandler.deleteJobDefinition(uuid);
            MysqlQueryHandler.executeUpdate(updateString);
        }catch(Exception e){ throw new IOException(e);}
    }

    //TODO: Implement this properly
    @Override
    public List<ExtendedJobDefinition> listAll() throws IOException {
        List<ExtendedJobDefinition> extendedJobDefinitionList = new ArrayList<>();
        String fetchString = MysqlQueryStringHandler.getUuidAndJobDefinition();

        ResultSet rs = null;
        try {
            rs = MysqlQueryHandler.executeQuery(fetchString);
            while(rs.next()){
                UUID uuid = UUID.fromString(rs.getString(1));
                JobDefinition def = MAPPER.readValue(rs.getString(2), JobDefinition.class);
                extendedJobDefinitionList.add(new ExtendedJobDefinition().uuid(uuid).definition(def));
            }
        }catch(SQLException e){ throw new IOException(e);}
        return extendedJobDefinitionList;
    }

    @Override
    public void close() throws IOException {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                throw new IOException(e);
            }
        }
    }
}
