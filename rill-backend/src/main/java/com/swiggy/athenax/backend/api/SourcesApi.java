package com.swiggy.athenax.backend.api;

import com.swiggy.athenax.backend.kafka.KafkaAdminOperator;
import com.swiggy.athenax.backend.models.TopicNameSchema;
import io.swagger.annotations.ApiParam;
import org.apache.kafka.common.errors.TopicExistsException;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;
import java.util.List;

@Path("/sources")

@io.swagger.annotations.Api(description = "the sources API")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2018-02-28T15:22:03.513+05:30")
public class SourcesApi {
    @POST
    @Path("/kafka/{sourceName}/register")
    // TODO: Add partitions and replication factor also as Optional here.
    // TODO: Map to self written exceptions
    @Produces({ "application/json" })
    public void createTopicIfUnavailable( @ApiParam(value = "KafkaAdminOperator Name",required=true) @PathParam("sourceName") String sourceName,
                                            @ApiParam(value = "The Schema" ,required=true) String schema,
            @Context SecurityContext securityContext)
            throws TopicExistsException, IOException{
        KafkaAdminOperator kafkaAdminOperator = new KafkaAdminOperator();
        kafkaAdminOperator.createTopicIfUnavailable(sourceName, schema);
    }

    @POST
    @Path("/kafka/{sourceName}/updateSchema")
    @Produces({"application/json"})
    public void updateTopicSchema(@ApiParam(value = "KafkaAdminOperator Name",required=true) @PathParam("sourceName") String sourceName,
                                  @ApiParam(value = "The Schema" ,required=true) String schema,
                                  @Context SecurityContext securityContext) throws IOException {
        KafkaAdminOperator kafkaAdminOperator = new KafkaAdminOperator();
        kafkaAdminOperator.updateSchema(sourceName, schema);
    }

    @GET
    @Path("/kafka/topicSchemaList")
    @Produces({"application/json"})
    public List<TopicNameSchema> fetchTopicSchemaList(@Context SecurityContext securityContext) throws IOException {
        KafkaAdminOperator kafkaAdminOperator = new KafkaAdminOperator();
        return kafkaAdminOperator.fetchTopicNameSchemaList();
    }

    @GET
    @Path("/kafka/{sourceName}/schema")
    @Produces({"application/json"})
    public TopicNameSchema fetchTopicSchema(@ApiParam(value = "Topic Name",required=true) @PathParam("sourceName") String sourceName,
                                            @Context SecurityContext securityContext) throws IOException {
        KafkaAdminOperator kafkaAdminOperator = new KafkaAdminOperator();
        return kafkaAdminOperator.fetchTopicNameSchema(sourceName);
    }
}
