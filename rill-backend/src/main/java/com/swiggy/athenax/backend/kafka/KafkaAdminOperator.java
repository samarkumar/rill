package com.swiggy.athenax.backend.kafka;

import com.swiggy.athenax.backend.models.TopicNameSchema;
import com.swiggy.athenax.backend.mysql.MysqlQueryHandler;
import com.swiggy.athenax.backend.mysql.MysqlQueryStringHandler;
import com.swiggy.athenax.backend.server.ServerContext;
import com.uber.athenax.backend.server.CoreContext;
import kafka.admin.AdminUtils;
import kafka.admin.RackAwareMode;
import org.apache.kafka.common.errors.TopicExistsException;
import org.I0Itec.zkclient.ZkClient;
import kafka.utils.ZkUtils;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class KafkaAdminOperator {
    private ZkClient zkClient;
    private ZkUtils zkUtils;
    private Connection mysqlConnection;

    public KafkaAdminOperator(){
        zkClient = CoreContext.INSTANCE.getZkClient();
        zkUtils = CoreContext.INSTANCE.getZkUtils();
        this.mysqlConnection = ServerContext.INSTANCE.getMysqlConnection();
    }

    public void createTopicIfUnavailable(String topicName, String schema) throws TopicExistsException, IOException{
        int noOfPartitions = 1;
        int noOfReplication = 1;
        if (AdminUtils.topicExists(zkUtils, topicName)) return;
        else{
            try {
                //TODO: Add transaction here
                AdminUtils.createTopic(zkUtils, topicName, noOfPartitions, noOfReplication, new Properties(),
                        AdminUtils.createTopic$default$6());
            } catch (RuntimeException e) {
                throw new IOException(e);
            }
            finally {
                if (zkClient != null) {
                    zkClient.close();
                }
            }
        }

        updateSchemainTopicSchemaTable(topicName, schema);
    }

    private void updateSchemainTopicSchemaTable(String topicName, String schema) throws IOException {
        String updateString = MysqlQueryStringHandler.insertOrUpdateTopicSchema(topicName, schema);
        MysqlQueryHandler.execute(updateString);
    }

    public void updateSchema(String sourceName, String schema) throws IOException {
        String updateString  = MysqlQueryStringHandler.updateTopicSchema(sourceName, schema);
        MysqlQueryHandler.execute(updateString);
    }

    public List<TopicNameSchema> fetchTopicNameSchemaList() throws IOException{
        String fetchString = MysqlQueryStringHandler.getTopicNameSchemaList();
        ResultSet rs = null;
        List<TopicNameSchema> topicNameSchemaList = new ArrayList<>();
        try {
            rs = MysqlQueryHandler.executeQuery(fetchString);
            while(rs.next()){
                TopicNameSchema topicNameSchema = new TopicNameSchema();
                topicNameSchema.setTopicName(rs.getString(1));
                topicNameSchema.setTopicSchema(rs.getString(2));
                topicNameSchemaList.add(topicNameSchema);
            }
        } catch (SQLException e) {
            throw new IOException(e);
        }
        return topicNameSchemaList;
    }

    public TopicNameSchema fetchTopicNameSchema(String topicName) throws IOException {
        TopicNameSchema topicNameSchema = new TopicNameSchema();
        String fetchString = MysqlQueryStringHandler.getTopicNameSchema(topicName);
        ResultSet rs = null;
        try {
            rs = MysqlQueryHandler.executeQuery(fetchString);
            if(rs.next()){
                topicNameSchema.setTopicName(rs.getString(1));
                topicNameSchema.setTopicSchema(rs.getString(2));
            }
        } catch (SQLException e) {
            throw new IOException(e);
        }
        return topicNameSchema;
    }
}
