package com.swiggy.athenax.backend.mysql;

import com.swiggy.athenax.backend.server.ServerContext;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public final class MysqlQueryHandler {
    private static Connection connection;
    public static final MysqlQueryHandler INSTANCE = new MysqlQueryHandler();

    private MysqlQueryHandler(){
        this.connection = ServerContext.INSTANCE.getMysqlConnection();
    }

    public static void execute(String queryString) throws IOException {
        try{
            Statement stmt = connection.createStatement();
            stmt.execute(queryString);
        }catch(Exception e){ throw new IOException(e);}
    }

    public static void executeUpdate(String queryString) throws IOException {
        try{
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(queryString);
        }catch(Exception e){ throw new IOException(e);}
    }

    public static ResultSet executeQuery(String queryString) throws IOException{
        try{
            Statement stmt = connection.createStatement();
            return stmt.executeQuery(queryString);
        }catch(Exception e){ throw new IOException(e);}
    }
}
