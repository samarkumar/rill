/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.swiggy.athenax.backend.server.jobs;

import com.swiggy.athenax.backend.api.JobDefinitionDesiredstate;
import com.swiggy.athenax.backend.server.AthenaXConfiguration;
import com.swiggy.athenax.backend.server.yarn.InstanceInfo;
import com.swiggy.athenax.backend.api.ExtendedJobDefinition;
import com.swiggy.athenax.backend.api.JobDefinition;
import com.swiggy.athenax.backend.server.InstanceStateUpdateListener;
import com.swiggy.athenax.backend.server.ServerContext;
import com.swiggy.athenax.vm.compiler.planner.JobCompilationResult;
import com.swiggy.athenax.vm.compiler.planner.Planner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class JobManager implements InstanceStateUpdateListener {
  private static final Logger LOG = LoggerFactory.getLogger(JobManager.class);
  private final JobStore jobStore;
  private final AthenaXConfiguration athenaXConfiguration;

  public JobManager(JobStore jobStore, Object catalogProvider, AthenaXConfiguration athenaXConfiguration) {
    this.jobStore = jobStore;
    this.athenaXConfiguration = athenaXConfiguration;
  }

  public UUID newJobUUID() {
    return UUID.randomUUID();
  }

  public void updateJob(UUID uuid, JobDefinition definition) throws IOException {
    jobStore.updateJob(uuid, definition);
  }

  public void removeJob(UUID uuid) throws IOException {
    jobStore.removeJob(uuid);
  }

  public List<ExtendedJobDefinition> listJobs() throws IOException {
    return jobStore.listAll();
  }

  public JobDefinition getJob(UUID jobUUID) throws IOException {
    return jobStore.get(jobUUID);
  }

  public JobCompilationResult compile(@Nonnull  JobDefinition job, JobDefinitionDesiredstate spec) throws Throwable {
    Planner planner = new Planner(job.getInputs(), job.getOutputs(), athenaXConfiguration);
    return planner.sql(job.getQuery(), Math.toIntExact(spec.getResource().getVcores()));
  }

  @Override
  public void onUpdatedInstances(ConcurrentHashMap<UUID, InstanceInfo> instances) {
    try {
      HashMap<UUID, JobDefinition> jobs = JobWatcherUtil.listJobs(jobStore);
      HealthCheckReport report = JobWatcherUtil.computeHealthCheckReport(jobs, instances);
      ServerContext.INSTANCE.watchdogPolicy().onHealthCheckReport(report);
    } catch (IOException e) {
      LOG.warn("Failed to run the health check policy ", e);
    }
  }
}
